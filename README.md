# Installation
```bash
pip3 install powerline-shell
```

# Fonts Powerline
```bash
apt install fonts-powerline
```

# Configuration
```bash
mkdir -p ~/.config/powerline-shell
cp config.json ~/.config/powerline-shell
```

## File .bashrc
```bash
# powerline-shell
function _update_ps1() {
   PS1=$(powerline-shell $?)
}

if [[ $TERM != linux && ! $PROMPT_COMMAND =~ _update_ps1 ]]; then
    PROMPT_COMMAND="_update_ps1; $PROMPT_COMMAND"
fi
```
